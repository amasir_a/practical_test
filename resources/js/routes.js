import VueRouter from 'vue-router'


import customer from './components/customer/index.vue';
import CreateCustomer from './components/customer/create.vue';
import EditCustomer from './components/customer/edit.vue';

let routes = [{
    name: 'home',
    path: '/',
    component: customer
},
    {
        name: 'create',
        path: '/create',
        component: CreateCustomer
    },
    {
        name: 'edit',
        path: '/edit/:id',
        component: EditCustomer
    }
];

export default new VueRouter({
    routes,
    mode: 'history'
})

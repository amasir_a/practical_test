<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" value="{{ csrf_token() }}" />
    <title>Practical Test</title>
    <link href="{{ mix('css/app.css') }}" type="text/css" rel="stylesheet" />
    <style src="cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css"></style>
</head>

<body>
<div id="app">
</div>
<script src="{{ mix('js/app.js') }}" type="text/javascript"></script>
</body>

</html>

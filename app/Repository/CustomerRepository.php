<?php


namespace App\Repository;


use App\Models\Customer;

class CustomerRepository implements CustomerInterface
{
    public function getAllCustomers()
    {
        return Customer::all();
    }

    public function getCustomerById($id)
    {
        return Customer::find($id);
    }

    public function create($collection = [])
    {
        $customer =  Customer::create($collection);
        foreach ($collection['contacts'] as $contact) {
            $customer->contacts()->create([
                'contact' => $contact['contact'],
            ]);
        }

        return $customer;
    }

    public function update($id, $collection = [])
    {
        $customer = Customer::find($id);
        $customer->update($collection);
        if ($collection['contacts']) {
            foreach ($collection['contacts'] as $contact) {
                $customer->contacts()->updateOrCreate([
                    'id' => $contact['id'],
                ], [
                    'contact' => $contact['contact'],
                ]);
            }
        }
        return $customer;
    }

    public function deleteCustomer($id)
    {
        return Customer::find($id)->delete();
    }

}

<?php


namespace App\Repository;



interface CustomerInterface
{
    public function getAllCustomers();

    public function getCustomerById($id);

    public function create($collection = [] );

    public function update( $id , $collection = [] );

    public function deleteCustomer($id);
}

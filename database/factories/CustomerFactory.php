<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'nic' => $this->faker->numberBetween(2000000000, 9999999999).'v',
            'address' => $this->faker->address,
        ];
    }
}

<?php

namespace Database\Factories;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\Factory;

class CustomerContactFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'contact' => $this->faker->phoneNumber(),
            'customer_id' => function () {
                return  self::factoryForModel(Customer::class)->create()->id;
            },
        ];
    }
}

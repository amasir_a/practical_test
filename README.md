## Installation & configuration:

install vendor:
```
composer install
```
Run migration:
```
php artisan migrate
php artisan DB:seed
```
Run Api:
```
php artisan serve
```

install node modules:
```
npm install
npm run dev
```
